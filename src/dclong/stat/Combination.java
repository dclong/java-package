package dclong.stat;

/**
 * Systematically generate combinations.
 */

import java.math.BigInteger;

public class Combination {
	/**
	 * An integer array for saving generated combination.
	 */
	private int[] a;
	/**
	 * The total number of elements.
	 */
	private int n;
	/**
	 * The number of elements to choose.
	 */
	private int r;
	/**
	 * The number of combinations left.
	 */
	private BigInteger numLeft;
	/**
	 * The total number of combinations.
	 */
	private BigInteger total;
	/**
	 * A constructor.
	 * @param n the total number of elements.
	 * @param r the number of elements to choose.
	 */
	public Combination(int n, int r) {
		if (r > n) {
			throw new IllegalArgumentException();
		}
		if (n < 1) {
			throw new IllegalArgumentException();
		}
		this.n = n;
		this.r = r;
		a = new int[r];
		total = choose(n,r);
		reset();
	}
	/**
	 * Reset this object to the starting state.
	 */
	public void reset() {
		for (int i = 0; i < a.length; ++i) {
			a[i] = i;
		}
		numLeft = new BigInteger(total.toString());
	}
	/**
	 * Return number of combinations not yet generated.
	 * @return number of combinations not yet generated.
	 */
	public BigInteger getNumLeft() {
		return numLeft;
	}
	/**
	 * Check whether there are more combinations.
	 * @return true if there are more combinations, false otherwise.
	 */
	public boolean hasMore() {
		return numLeft.compareTo(BigInteger.ZERO) == 1;
	}

	/**
	 * Return the total number of combinations.
	 * @return the total number of combinations. 
	 */
	public BigInteger getTotal() {
		return total;
	}

	/**
	 * Calculates factorial of nonnegative integer.
	 * @param n a nonnegative integer.
	 * @return the factorial of the given integer.
	 */
	public static BigInteger getFactorial(int n) {
		BigInteger fact = BigInteger.ONE;
		for (int i = n; i > 1; i--) {
			fact = fact.multiply(new BigInteger(Integer.toString(i)));
		}
		return fact;
	}
	/**
	 * Calculates number of unique combinations.
	 * @param n the total number of elements.
	 * @param r the number of elements to choose.
	 * @return the number of unique combinations.
	 */
	public static BigInteger choose(int n, int r){
		BigInteger ch = getFactorial(n).divide(getFactorial(r).multiply(getFactorial(n-r)));
		return ch;
	}
	/**
	 * Generate next combination (algorithm from Rosen p. 286)
	 * @return the next combination.
	 */
	public int[] nextCombination() {
		if (numLeft.equals(total)) {
			numLeft = numLeft.subtract(BigInteger.ONE);
			return a;
		}
		int i = r - 1;
		while (a[i] == n - r + i) {
			i--;
		}
		a[i] = a[i] + 1;
		for (int j = i + 1; j < r; j++) {
			a[j] = a[i] + j - i;
		}
		numLeft = numLeft.subtract(BigInteger.ONE);
		return a;
	}
	/**
	 * Get the first n combinations, where n is an integer.
	 * @param n the number of combinations to return.
	 * @return the first n combinations.
	 */
	public int[][] getCombinations(int n){
		reset();
		int[][] cs = new int[n][r];
		for(int rowIndex=0; rowIndex<n; ++rowIndex){
			System.arraycopy(nextCombination(),0,cs[rowIndex],0,r);
		}
		return cs;
	}
}
