package dclong.stat;

import org.apache.commons.math3.random.RandomDataImpl;


public class Random {
	/**
	 * 
	 * @param rng
	 * @param weight
	 * @return
	 */
	public static int sample(RandomDataImpl rng,double[] probability){
		double u = rng.nextUniform(0, 1);
		for(int i=probability.length-1; i>=0; --i){
			if(u<=probability[i]){
				return i;
			}else{
				u -= probability[i];
			}
		}
		return (int) rng.nextUniform(0, probability.length);
	}
	
//	private static int sampleOne(RandomDataImpl rng, double[] scaledWeight){
//		double u = rng.nextUniform(0, 1);
//		for(int i=scaledWeight.length-1; i>=0; --i){
//			if(u<=scaledWeight[i]){
//				return i;
//			}else{
//				u -= scaledWeight[i];
//			}
//		}
//		return (int) rng.nextUniform(0, scaledWeight.length);
//	}
	
	public static int[] sample(RandomDataImpl rng, int k, double[] probability){
		int[] sam = new int[k];
		for(int i=k-1; i>=0; --i){
			sam[i] = sample(rng,probability);
		}
		return sam;
	}
}
