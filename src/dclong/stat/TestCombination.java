package dclong.stat;

import dclong.util.Timer;

public class TestCombination {
	public static void main(String[] args){
		Timer timer = new Timer();
		Combination comb = new Combination(6,3);
		timer.start();
		dclong.io.Console.print(comb.nextCombination());
		System.out.print("\n");
		dclong.io.Console.print(comb.nextCombination());
		System.out.print("\n");
		System.out.println("---------------------");
		int[][] cs = comb.getCombinations(comb.getTotal().intValue());
		timer.stop();
		int[][] cs2 = new int[cs.length/2][cs[0].length*2];
		for(int i=0; i<cs2.length; ++i){
			for(int j=0; j<cs2[0].length/2; ++j){
				cs2[i][j] = cs[i][j];
			}
			for(int j=cs2[0].length/2; j<cs2[0].length; ++j){
				cs2[i][j] = cs[cs.length-1-i][j-cs2[0].length/2];
			}
		}
		dclong.io.Console.print(cs2);
		timer.printSeconds("generating combinations");
	}
}
