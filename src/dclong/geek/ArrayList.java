package dclong.geek;

import java.lang.reflect.Field;

public class ArrayList {

    public static int getCapacity(java.util.ArrayList<?> l) throws Exception {
        Field dataField = ArrayList.class.getDeclaredField("elementData");
        dataField.setAccessible(true);
        return ((Object[]) dataField.get(l)).length;
    }
}