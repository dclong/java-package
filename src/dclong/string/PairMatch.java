package dclong.string;

import java.util.ArrayList;

public class PairMatch {
	public static String pairMatchReplace(String leftDelimiter,String rightDelimiter,String replacement,String aString,int startIndex){
		int[][] index = pairMatch(leftDelimiter,rightDelimiter,aString,startIndex);
		System.out.println(index.length + " occurrences are found.");
		if(index.length==0){
			return aString;
		}
		String bString = "";
		int currentStart = 0;
		for(int i=0; i<index.length; i++){
			if(index[i][1]>currentStart){
				bString += aString.substring(currentStart,index[i][1]);
			}
			bString += replacement;
			currentStart = index[i][2] + 1;
		}
		int length = aString.length();
		if(length>currentStart){
			bString += aString.substring(currentStart,length);
		}
		return bString;
	}
	/**
	 * Find all pair matches.
	 * @param leftDelimiter the left delimiter (can be a string).
	 * @param rightDelimiter the right delimiter (can be a string).
	 * @param aString the string in which pair matches are to be found.
	 * @param startIndex the start index from where the search is performed.
	 * @return a 2-D array of 3 column with the first column be the starting indexes,
	 * the second column be the left indexes (i.e. first index of the substring found)
	 * and the third column be the right indexes (i.e. the last index of the substring).
	 */
	public static int[][] pairMatch(String leftDelimiter, String rightDelimiter, String aString,int startIndex){
		ArrayList<Integer> resultIndex = new ArrayList<Integer>();
		//match one by one
		int [] resultIndexOnce = pairMatchOnce(leftDelimiter,rightDelimiter,aString,startIndex);
		while(resultIndexOnce[1]>=0 && resultIndexOnce[2]>=0){
			resultIndex.add(resultIndexOnce[0]);
			resultIndex.add(resultIndexOnce[1]);
			resultIndex.add(resultIndexOnce[2]);
			resultIndexOnce = pairMatchOnce(leftDelimiter,rightDelimiter,aString,resultIndexOnce[2]+1);
		}
		//reshape the ArrayList to a 2-D array
		if(resultIndex.size()>0){
			int[][] finalResult = new int[resultIndex.size()/3][3];
			for(int i=0; i<finalResult.length; i++){
				int arrayListIndex = 3*i;
				finalResult[i][0] = resultIndex.get(arrayListIndex);
				finalResult[i][1] = resultIndex.get(arrayListIndex+1);
				finalResult[i][2] = resultIndex.get(arrayListIndex+2);
			}
			return finalResult;
		}
		return null;
	}
	/**
	 * Find the first pair match.
	 * @param leftDelimiter the left delimiter (can be a string).
	 * @param rightDelimiter the right delimiter (can be a string).
	 * @param aString the string in which pair matches are to be found.
	 * @param startIndex the start index from where the search is performed.
	 * @return an array of length 3 with the first element be the starting index,
	 * the second element be the left index (i.e. first index of the substring found)
	 * and the third element be the right index (i.e. the last index of the substring).
	 */
	public static int[] pairMatchOnce(String leftDelimiter, String rightDelimiter, String aString, int startIndex){
		//an array of length 3 whose first element is the start index, 
		//second element is the left index and the last element is the right index
		int[] resultIndex = {startIndex,-1,-1};
		//length of the left and right delimiters for future frequent use
		int leftDelimiterLength = leftDelimiter.length();
		int rightDelimiterLength = rightDelimiter.length();
		//when left delimiter and the right delimiter are the same
		if(leftDelimiter.equals(rightDelimiter)){
			resultIndex[1] = aString.indexOf(leftDelimiter, startIndex);
			if(resultIndex[1]>=0){
				resultIndex[2] = aString.indexOf(rightDelimiter,resultIndex[1]+leftDelimiterLength);
			}
			if(resultIndex[2]>=0){
				resultIndex[2] += rightDelimiterLength - 1;
			}
			return resultIndex;
		}
		//when left delimiter and the right delimiter are not the same
		resultIndex[1] = aString.indexOf(leftDelimiter,startIndex);
		if(resultIndex[1]<0){
			resultIndex[2] = aString.indexOf(rightDelimiter,startIndex);
			if(resultIndex[2]>=0){
				resultIndex[2] += rightDelimiterLength - 1;
			}
			return resultIndex;
		}
		//number of left delimiters to be matched, stop until 0
		int toBeMatched = 1;
		startIndex = resultIndex[1] + leftDelimiterLength;
		int leftIndex, rightIndex;
		int aStringLength = aString.length();
		while(startIndex<aStringLength){
			rightIndex = aString.indexOf(rightDelimiter,startIndex);
			if(rightIndex<0){
				return resultIndex;
			}
			leftIndex = aString.indexOf(leftDelimiter,startIndex);
			if(leftIndex>=0 && leftIndex<rightIndex){
				toBeMatched++;
				startIndex = leftIndex + leftDelimiterLength;
			}else{
				toBeMatched--;
				if(toBeMatched==0){
					resultIndex[2] = rightIndex;
					break;
				}
				startIndex = rightIndex + rightDelimiterLength;
			}
		}
		if(resultIndex[2]>=0){
			resultIndex[2] += rightDelimiterLength - 1;
		}
		return resultIndex;
	}
}
