package dclong.string;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
//TODO
// there is a bug
// it is probably because of the way you used builder.replace
// it is probaly not what you thought it were
// not sure what caused the problem???
// add dynamic support instead of just static replacing
// general idea:
// 1. parse the old string format: prefix, #k, #m, #l
// 2. find string match the format, extract parameters and sort them according to #n
// 3. \{ need to be taken care of when count paired }
// when find paired {, there need to be two nested counting, one for matching {,
// the other for matching the number of parameters
// 4. replace #n with corresponding parameter in new string 
public class LatexParser {
	
	private static class Rep implements Comparable<Rep>{
		protected String oldStr;
		protected String newStr;
		protected int priority;
		protected boolean dynamic;
		public Rep(String oldStr,String newStr,int priority){
			this.oldStr = oldStr;
			this.newStr = newStr;
			this.priority = priority;
		}
		@Override
		public int compareTo(Rep other) {
			return priority - other.priority;
		}
	}
	
//	private static class Arg implements Comparable<Arg>{
//		protected String arg;
//		protected int order;
//		public Arg(String arg, int order){
//			this.arg = arg;
//			this.order = order;
//		}
//	}
//	
//	private static class Command{
//		protected String cmd;
//		protected Arg[] args;
//		public Command(){
//			
//		}
//	}
	
	public static void main(String[] args){
		if(args.length==0){
			System.err.println("A tex file must be specified.");
		}
		String texFile = args[0];
		int priority = 0;
		if(args.length>=2){
			try{
				priority = Integer.parseInt(args[1]);
			}
			catch (NumberFormatException e){
				System.err.println("Illegal input for priority.");
			}
		}
		String workingDirectory = System.getProperty("user.dir");
		String repFile = new File(workingDirectory,"latex-parser.txt").getPath();
		System.out.println(replace(texFile,repFile,priority));
	}
	
	private static String replace(String texFile, String repFile,int priority){
		String content = "";
		try {
			content = new Scanner(new File(texFile)).useDelimiter("\\Z").next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.err.println("File \"" + texFile + "\" is not found.");
			return null;
		}
		Rep[] reps = readRep(repFile,priority);
		Arrays.sort(reps);
		StringBuilder builder = new StringBuilder(content);
		for(int i=0; i<reps.length; ++i){
			builder = replace(builder,reps[i]);
		}
		return builder.toString();
	}
	
	private static StringBuilder replace(StringBuilder builder, Rep rep){
//		if(rep.dynamic){
//		}
		int startIndex = builder.lastIndexOf(rep.oldStr);
		int oldLength = rep.oldStr.length();
		while(startIndex>=0){
			builder.replace(startIndex, startIndex + oldLength, rep.newStr);
			startIndex = builder.lastIndexOf(rep.oldStr);
		}
		return builder;
	}
	
//	private static Arg[] dessemble(String str){
//		ArrayList<Arg> args = new ArrayList<Arg>(5);
//		int flag = str.indexOf('{');
//		String prefix = str.substring(0,flag);
//		dessemble(str,flag,args);
//	}
//	
//	private static void dessemble(String str, int fromIndex,ArrayList<Arg> args){
//		int startIndex = str.indexOf('{',fromIndex);
//		if(startIndex>=0){
//			int toMatch = 1;
//			
//		}
//	}
	
	private static Rep[] readRep(String file,int priority){
		ArrayList<Rep> repList = new ArrayList<Rep>(1500);
		Scanner in = null;
		try {
			in = new Scanner(new File(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.err.println("File \"" + file + "\" is not found.");
			return null;
		}
		in.useDelimiter("\n");
		int count = 0;
		while(in.hasNext()){
			count ++;
			String line = in.next();
			if(line.startsWith("#")||line.equals("")){//skip comment lines and blank lines
				continue;
			}
			String[] args = line.split(",");
			if(args.length==3){
				try{
					int p = Integer.parseInt(args[2]);
					if(p<=priority){
						repList.add(new Rep(args[0].replace('$', '\n'),args[1].replace('$', '\n'),p));
					}
				}
				catch (NumberFormatException e){
					e.printStackTrace();
					System.out.println("Line " + count + " has illegal value for priority.");
				}
				continue;
			}
			System.out.println("Line " + count + " does not have the right number of arguments");
		}
		Rep[] reps = new Rep[repList.size()];
		return repList.toArray(reps);
	}
}
