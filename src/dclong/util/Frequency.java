package dclong.util;

public class Frequency<T> {
	private T value;
	private int frequency;
	
	public Frequency(T obj,int freq){
		this.value = obj;
		this.frequency = freq;
	}
	
	public T getValue(){
		return value;
	}
	
	public int getFreq(){
		return frequency;
	}
}
