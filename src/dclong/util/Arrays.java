package dclong.util;

import java.util.ArrayList;

/**
 * A class offers complementary functionality to java.util.Arrays.
 * 
 * @author adu
 * 
 */
public class Arrays {
	/**
	 * Copy a 2-D array to another 2-D array fast.
	 * 
	 * @param from
	 *            the source array.
	 * @param to
	 *            the destination array.
	 */
	public static void copy(Object[][] from, Object[][] to) {
		for (int i = 0; i < from.length; ++i) {
			System.arraycopy(from, 0, to, 0, to[i].length);
		}
	}

	/**
	 * Copy a 2-D array to another 2-D array fast.
	 * 
	 * @param from
	 *            the source array.
	 * @param to
	 *            the destination array.
	 */
	public static void copy(double[][] from, double[][] to) {
		for (int i = 0; i < from.length; ++i) {
			System.arraycopy(from, 0, to, 0, to[i].length);
		}
	}

	/**
	 * Copy a 2-D array to another 2-D array fast.
	 * 
	 * @param from
	 *            the source array.
	 * @param to
	 *            the destination array.
	 */
	public static void copy(float[][] from, float[][] to) {
		for (int i = 0; i < from.length; ++i) {
			System.arraycopy(from, 0, to, 0, to[i].length);
		}
	}

	/**
	 * Copy a 2-D array to another 2-D array fast.
	 * 
	 * @param from
	 *            the source array.
	 * @param to
	 *            the destination array.
	 */
	public static void copy(int[][] from, int[][] to) {
		for (int i = 0; i < from.length; ++i) {
			System.arraycopy(from, 0, to, 0, to[i].length);
		}
	}

	/**
	 * Copy a 2-D array to another 2-D array fast.
	 * 
	 * @param from
	 *            the source array.
	 * @param to
	 *            the destination array.
	 */
	public static void copy(boolean[][] from, boolean[][] to) {
		for (int i = 0; i < from.length; ++i) {
			System.arraycopy(from, 0, to, 0, to[i].length);
		}
	}

	/**
	 * Copy a 2-D array to another 2-D array fast.
	 * 
	 * @param from
	 *            the source array.
	 * @param to
	 *            the destination array.
	 */
	public static void copy(String[][] from, String[][] to) {
		for (int i = 0; i < from.length; ++i) {
			System.arraycopy(from, 0, to, 0, to[i].length);
		}
	}

	/**
	 * Copy specified columns of a 2-d array to another 2-d array.
	 * 
	 * @param from
	 *            the source array.
	 * @param to
	 *            the destination array.
	 * @param columnIndex
	 *            indices of columns to be copied.
	 */
	public static void copy(double[][] from, double[][] to, int[] columnIndex) {
		int numberOfRows = to.length;
		int numberOfColumns = to[0].length;
		for (int i = 0; i < numberOfRows; ++i) {
			for (int j = 0; j < numberOfColumns; ++j) {
				to[i][j] = from[i][columnIndex[j]];
			}
		}
	}

	/**
	 * Copy specified columns of a 2-d array to another 2-d array.
	 * 
	 * @param from
	 *            the source array.
	 * @param to
	 *            the destination array.
	 * @param columnIndex
	 *            indices of columns to be copied.
	 */
	public static void copy(float[][] from, float[][] to, int[] columnIndex) {
		int numberOfRows = to.length;
		int numberOfColumns = to[0].length;
		for (int i = 0; i < numberOfRows; ++i) {
			for (int j = 0; j < numberOfColumns; ++j) {
				to[i][j] = from[i][columnIndex[j]];
			}
		}
	}

	/**
	 * Copy specified columns of a 2-d array to another 2-d array.
	 * 
	 * @param from
	 *            the source array.
	 * @param to
	 *            the destination array.
	 * @param columnIndex
	 *            indices of columns to be copied.
	 */
	public static void copy(int[][] from, int[][] to, int[] columnIndex) {
		int numberOfRows = to.length;
		int numberOfColumns = to[0].length;
		for (int i = 0; i < numberOfRows; ++i) {
			for (int j = 0; j < numberOfColumns; ++j) {
				to[i][j] = from[i][columnIndex[j]];
			}
		}
	}

	/**
	 * Copy specified columns of a 2-d array to another 2-d array.
	 * 
	 * @param from
	 *            the source array.
	 * @param to
	 *            the destination array.
	 * @param columnIndex
	 *            indices of columns to be copied.
	 */
	public static void copy(boolean[][] from, boolean[][] to, int[] columnIndex) {
		int numberOfRows = to.length;
		int numberOfColumns = to[0].length;
		for (int i = 0; i < numberOfRows; ++i) {
			for (int j = 0; j < numberOfColumns; ++j) {
				to[i][j] = from[i][columnIndex[j]];
			}
		}
	}

	/**
	 * Copy specified columns of a 2-d array to another 2-d array.
	 * 
	 * @param from
	 *            the source array.
	 * @param to
	 *            the destination array.
	 * @param columnIndex
	 *            indices of columns to be copied.
	 */
	public static void copy(String[][] from, String[][] to, int[] columnIndex) {
		int numberOfRows = to.length;
		int numberOfColumns = to[0].length;
		for (int i = 0; i < numberOfRows; ++i) {
			for (int j = 0; j < numberOfColumns; ++j) {
				to[i][j] = from[i][columnIndex[j]];
			}
		}
	}

	/**
	 * Check two 2-d arrays are equal to each other.
	 * 
	 * @param aArray
	 *            a 2-d array.
	 * @param bArray
	 *            another 2-d array.
	 * @return true if the the 2 arrays equal each other, and false otherwise.
	 */
	public static boolean equals(int[][] aArray, int[][] bArray) {
		if (aArray == bArray) {
			return true;
		}
		if (aArray.length != bArray.length) {
			return false;
		}
		for (int i = 0; i < aArray.length; ++i) {
			if (!java.util.Arrays.equals(aArray[i], bArray[i])) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Check two 2-d arrays are equal to each other.
	 * 
	 * @param aArray
	 *            a 2-d array.
	 * @param bArray
	 *            another 2-d array.
	 * @return true if the the 2 arrays equal each other, and false otherwise.
	 */
	public static boolean equals(boolean[][] aArray, boolean[][] bArray) {
		if (aArray == bArray) {
			return true;
		}
		if (aArray.length != bArray.length) {
			return false;
		}
		for (int i = 0; i < aArray.length; ++i) {
			if (!java.util.Arrays.equals(aArray[i], bArray[i])) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Check two 2-d arrays are equal to each other.
	 * 
	 * @param aArray
	 *            a 2-d array.
	 * @param bArray
	 *            another 2-d array.
	 * @return true if the the 2 arrays equal each other, and false otherwise.
	 */
	public static boolean equals(String[][] aArray, String[][] bArray) {
		if (aArray == bArray) {
			return true;
		}
		if (aArray.length != bArray.length) {
			return false;
		}
		for (int i = 0; i < aArray.length; ++i) {
			if (!java.util.Arrays.equals(aArray[i], bArray[i])) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Check two 2-d arrays are equal to each other.
	 * 
	 * @param aArray
	 *            a 2-d array.
	 * @param bArray
	 *            another 2-d array.
	 * @return true if the the 2 arrays equal each other, and false otherwise.
	 */
	public static boolean equals(Object[][] aArray, Object[][] bArray) {
		if (aArray == bArray) {
			return true;
		}
		if (aArray.length != bArray.length) {
			return false;
		}
		for (int i = 0; i < aArray.length; ++i) {
			if (!java.util.Arrays.equals(aArray[i], bArray[i])) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Check two double arrays are equal to each other. There's a similar
	 * implementation in java.util.Arrays, but it's no good, because it offers
	 * no error range.
	 * 
	 * @param aArray
	 *            a double array.
	 * @param bArray
	 *            another double array.
	 * @param error
	 *            a positive value which is the maximum error allowed.
	 * @return true if the two arrays equal each other, and false otherwise.
	 */
	public static boolean equals(double[] aArray, double[] bArray, double error) {
		if (aArray == bArray) {
			return true;
		}
		if (aArray.length != bArray.length) {
			return false;
		}
		for (int i = 0; i < aArray.length; ++i) {
			if (Math.abs(aArray[i] - bArray[i]) > error) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Check two float arrays are equal to each other. There's a similar
	 * implementation in java.util.Arrays, but it's no good, because it offers
	 * no error range.
	 * 
	 * @param aArray
	 *            a float array.
	 * @param bArray
	 *            another float array.
	 * @param error
	 *            a positive value which is the maximum error allowed.
	 * @return true if the two arrays equal each other, and false otherwise.
	 */
	public static boolean equals(float[] aArray, float[] bArray, float error) {
		if (aArray == bArray) {
			return true;
		}
		if (aArray.length != bArray.length) {
			return false;
		}
		error = Math.abs(error);
		for (int i = 0; i < aArray.length; ++i) {
			if (Math.abs(aArray[i] - bArray[i]) > error) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Check two 2-d arrays are equal to each other. The
	 * 
	 * @param aArray
	 *            a 2-d array.
	 * @param bArray
	 *            another 2-d array.
	 * @param error
	 *            the maximum error that is allowed.
	 * @return true if the the 2 arrays equal each other, and false otherwise.
	 */
	public static boolean equals(double[][] aArray, double[][] bArray,
			double error) {
		if (aArray == bArray) {
			return true;
		}
		if (aArray.length != bArray.length) {
			return false;
		}
		for (int i = 0; i < aArray.length; ++i) {
			if (!equals(aArray[i], bArray[i], error)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Check two 2-d arrays are equal to each other. The
	 * 
	 * @param aArray
	 *            a 2-d array.
	 * @param bArray
	 *            another 2-d array.
	 * @param error
	 *            the maximum error that is allowed.
	 * @return true if the the 2 arrays equal each other, and false otherwise.
	 */
	public static boolean equals(float[][] aArray, float[][] bArray, float error) {
		if (aArray == bArray) {
			return true;
		}
		if (aArray.length != bArray.length) {
			return false;
		}
		for (int i = 0; i < aArray.length; ++i) {
			if (!equals(aArray[i], bArray[i], error)) {
				return false;
			}
		}
		return true;
	}

	public static double sum(double[] array) {
		double s = 0;
		for (int i = 0; i < array.length; ++i) {
			s += array[i];
		}
		return s;
	}

	public static double sum(double[] array, int[] index) {
		double s = 0;
		for (int i = 0; i < index.length; ++i) {
			s += array[index[i]];
		}
		return s;
	}

	public static double sum(double[] array, ArrayList<Integer> index) {
		double s = 0;
		int size = index.size();
		for (int i = 0; i < size; ++i) {
			s += array[index.get(i)];
		}
		return s;
	}

	public static double sum(double[][] array) {
		double s = 0;
		for (int i = 0; i < array.length; ++i) {
			s += sum(array[i]);
		}
		return s;
	}

	// public static double rowSums(double[][] array){
	//
	// }

	public static double sum(float[] array) {
		double s = 0;
		for (int i = 0; i < array.length; ++i) {
			s += array[i];
		}
		return s;
	}

	public static double sum(float[] array, int[] index) {
		double s = 0;
		for (int i = 0; i < index.length; ++i) {
			s += array[index[i]];
		}
		return s;
	}

	public static double sum(float[] array, ArrayList<Integer> index) {
		double s = 0;
		int size = index.size();
		for (int i = 0; i < size; ++i) {
			s += array[index.get(i)];
		}
		return s;
	}

	public static double sum(float[][] array) {
		double s = 0;
		for (int i = 0; i < array.length; ++i) {
			s += sum(array[i]);
		}
		return s;
	}

	public static double sum(int[] array) {// return double because of its wide
											// range
		double s = 0;
		for (int i = 0; i < array.length; ++i) {
			s += array[i];
		}
		return s;
	}

	public static double sum(int[] array, int[] index) {
		double s = 0;
		for (int i = 0; i < index.length; ++i) {
			s += array[index[i]];
		}
		return s;
	}

	public static double sum(int[] array, ArrayList<Integer> index) {
		double s = 0;
		int size = index.size();
		for (int i = 0; i < size; ++i) {
			s += array[index.get(i)];
		}
		return s;
	}

	public static double sum(int[][] array) {
		double s = 0;
		for (int i = 0; i < array.length; ++i) {
			s += sum(array[i]);
		}
		return s;
	}

	public static int sum(boolean[] array) {
		int s = 0;
		for (int i = 0; i < array.length; ++i) {
			if (array[i]) {
				s++;
			}
		}
		return s;
	}

	public static int sum(boolean[] array, int[] index) {
		int s = 0;
		for (int i = 0; i < index.length; ++i) {
			if (array[index[i]]) {
				s++;
			}
		}
		return s;
	}

	public static int sum(boolean[] array, ArrayList<Integer> index) {
		int s = 0;
		int size = index.size();
		for (int i = 0; i < size; ++i) {
			if (array[index.get(i)]) {
				s++;
			}
		}
		return s;
	}

	public static long sum(boolean[][] array) {
		long s = 0;
		for (int i = 0; i < array.length; ++i) {
			s += sum(array[i]);
		}
		return s;
	}

	/**
	 * Get the 1-D index of an element in a 2-D rectangular array.
	 * 
	 * @param rowIndex
	 *            the row index of the element.
	 * @param columnIndex
	 *            the column index of the element.
	 * @param numberOfColumns
	 *            the number of columns of the regular 2-D array.
	 * @return the 1-D index of the element.
	 */
	public static int uniIndex(int rowIndex, int columnIndex,
			int numberOfColumns) {
		return rowIndex * numberOfColumns + columnIndex;
	}

	/**
	 * Split a 2-D integer array to different classes according to its values.
	 * 
	 * @param array
	 *            a 2-D integer array.
	 * @return an ArrayList of ArrayList of points.
	 */
	public static ArrayList<ArrayList<java.awt.Point>> splitArray(int[][] array) {
		// average dimension of the 2-D array,
		// this is used as the initial capacity of the ArrayList
		// this might use more space, but can reduce the times of reallocation.
		int aveDim = (array.length + array[0].length) / 2;
		// initialize an ArrayList of ArrayList<Integer> for storing indexes of
		// different classes
		ArrayList<ArrayList<java.awt.Point>> comList = new ArrayList<ArrayList<java.awt.Point>>(
				aveDim);
		// initialize an ArrayList of Integer for storing current existing
		// classes
		ArrayList<Integer> classIDs = new ArrayList<Integer>(aveDim);
		// split the 2-D array
		for (int i = 0; i < array.length; ++i) {
			for (int j = 0; j < array[0].length; ++j) {
				java.awt.Point pt = new java.awt.Point(i, j);
				// class ID of current point
				int cid = array[i][j];
				// find the index of the class id in classIDs
				int index = classIDs.indexOf(cid);
				if (index >= 0) {// an ArrayList<Point> for the class already
									// exists
					comList.get(index).add(pt);
				} else {// an ArrayList<Point> for the class doesn't exist
					ArrayList<java.awt.Point> aList = new ArrayList<java.awt.Point>(
							aveDim);
					aList.add(pt);
					comList.add(aList);
					// push the current class id into the classIDs
					classIDs.add(cid);
				}
			}
		}
		return comList;
	}

	public static ArrayList<ArrayList<Integer>> splitArray(int[] array,
			int capacity1, int capacity2) {
		ArrayList<ArrayList<Integer>> partition = new ArrayList<ArrayList<Integer>>(
				capacity1);
		ArrayList<Integer> classIDs = new ArrayList<Integer>(capacity1);
		for (int i = 0; i < array.length; ++i) {
			int cid = array[i];
			int index = classIDs.indexOf(cid);
			if (index >= 0) {
				partition.get(index).add(i);
			} else {
				ArrayList<Integer> list = new ArrayList<Integer>(capacity2);
				list.add(i);
				partition.add(list);
				classIDs.add(cid);
			}
		}
		return partition;
	}

	public static ArrayList<ArrayList<Integer>> splitArray(int[] array) {
		int capacity = (int) Math.ceil(Math.sqrt(array.length));
		return splitArray(array, capacity, capacity);
	}

	public static double[][] toTwoD(double[] array,int rowNumber, int colNumber,boolean byRow){
		if(byRow){
			return toTwoDByRow(array,rowNumber,colNumber);
		}
		return toTwoDByColumn(array,rowNumber,colNumber);
	}
	
	private static double[][] toTwoDByRow(double[] array, int rowNumber,
			int columnNumber) {
		double[][] a = new double[rowNumber][columnNumber];
		int index = 0;
		for (int rowIndex = 0; rowIndex < rowNumber; ++rowIndex) {
			for (int colIndex = 0; colIndex < columnNumber; ++colIndex) {
				a[rowIndex][colIndex] = array[index];
				++ index;
			}
		}
		return a;
	}

	private static double[][] toTwoDByColumn(double[] array, int rowNumber,
			int columnNumber) {
		double[][] a = new double[rowNumber][columnNumber];
		int index = 0;
		for (int colIndex = 0; colIndex < columnNumber; ++colIndex) {
			for (int rowIndex = 0; rowIndex < rowNumber; ++rowIndex) {
				a[rowIndex][colIndex] = array[index];
				++ index;
			}
		}
		return a;
	}

	// public static double[] toOneD(double[][] array){
	//
	// }

	public static double[][][] toThreeD(double[] array, int d1, int d2, int d3) {
		double[][][] a = new double[d1][d2][d3];
		for (int i = 0; i < d1; ++i) {
			for (int j = 0; j < d2; ++j) {
				for (int k = 0; k < d3; ++k) {
					a[i][j][k] = array[i * d2 * d3 + j * d3 + k];
				}
			}
		}
		return a;
	}

	// ---------------------- vector operations --------------------------------
	public static double[] add(double[] a1, double a2[]) {
		double[] sum = new double[a1.length];
		for (int i = a1.length - 1; i >= 0; --i) {
			sum[i] = a1[i] + a2[i];
		}
		return sum;
	}

	public static double[] add(double[] a1, double scalor) {
		double[] sum = new double[a1.length];
		for (int i = a1.length - 1; i >= 0; --i) {
			sum[i] = a1[i] + scalor;
		}
		return sum;
	}

	public static double[] subtract(double[] a1, double[] a2) {
		double[] diff = new double[a1.length];
		for (int i = a1.length - 1; i >= 0; --i) {
			diff[i] = a1[i] - a2[i];
		}
		return diff;
	}

	public static double[] multiply(double[] a1, double[] a2) {
		double[] prod = new double[a1.length];
		for (int i = a1.length - 1; i >= 0; --i) {
			prod[i] = a1[i] * a2[i];
		}
		return prod;
	}

	public static double[] multiply(double[] a1, double scalor) {
		double[] prod = new double[a1.length];
		for (int i = a1.length - 1; i >= 0; --i) {
			prod[i] = a1[i] * scalor;
		}
		return prod;
	}

	public static double[] divide(double[] a1, double[] a2) {
		double[] quo = new double[a1.length];
		for (int i = a1.length - 1; i >= 0; --i) {
			quo[i] = a1[i] / a2[i];
		}
		return quo;
	}

	public static double[] divide(double[] a1, double scalor) {
		double[] quo = new double[a1.length];
		for (int i = a1.length - 1; i >= 0; --i) {
			quo[i] = a1[i] / scalor;
		}
		return quo;
	}

	public static double[] divide(double quotient[], double[] a1, double scalor) {
		for (int i = a1.length - 1; i >= 0; --i) {
			quotient[i] = a1[i] / scalor;
		}
		return quotient;
	}

	public static int[] sequence(int from, int to, int by) {
		int size = (to - from) / by + 1;
		int[] seq = new int[size];
		for (int i = 0; i < size; ++i) {
			seq[i] = from;
			from += by;
		}
		return seq;
	}

	public static double[] sequence(double from, double to, double by) {
		int size = (int) ((to - from) / by) + 1;
		double[] seq = new double[size];
		for (int i = 0; i < size; ++i) {
			seq[i] = from;
			from += by;
		}
		return seq;
	}

	public static double[][] cbind(double[] a1, double[] a2) {
		double[][] a = new double[a1.length][2];
		for (int i = a1.length - 1; i >= 0; --i) {
			a[i][0] = a1[i];
			a[i][1] = a2[i];
		}
		return a;
	}

	public static int[][] cbind(int[] a1, int[] a2) {
		int[][] a = new int[a1.length][2];
		for (int i = a1.length - 1; i >= 0; --i) {
			a[i][0] = a1[i];
			a[i][1] = a2[i];
		}
		return a;
	}

	public static double[][] cbind(int[] a1, double[] a2) {
		double[][] a = new double[a1.length][2];
		for (int i = a1.length - 1; i >= 0; --i) {
			a[i][0] = a1[i];
			a[i][1] = a2[i];
		}
		return a;
	}

	public static double[][] cbind(double[] a1, int[] a2) {
		double[][] a = new double[a1.length][2];
		for (int i = a1.length - 1; i >= 0; --i) {
			a[i][0] = a1[i];
			a[i][1] = a2[i];
		}
		return a;
	}
}
