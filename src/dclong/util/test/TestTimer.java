package dclong.util.test;

import dclong.util.Timer;

public class TestTimer {
	public static void main(String[] args) throws InterruptedException{
		Timer timer = new Timer();
		timer.start();
		Thread.sleep(1000);
		timer.stop();
		timer.printSeconds("sleeping");
	}
}
