package dclong.util.test;

import org.apache.commons.math3.random.RandomDataImpl;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well44497b;

import dclong.util.Arrays;
import dclong.util.Timer;

public class TestArrayRandom {

	public static void main(String[] args){
		
		double[] weight = new double[10];
		initializeWeight(weight);
		int n = 100000;
		RandomGenerator rg = new Well44497b();
		RandomDataImpl rng = new RandomDataImpl(rg);
		dclong.util.Timer timer = new dclong.util.Timer();
		timer.start();
		double[] proportion = sample(rng,weight,n);
		timer.end();
		timer.printSeconds("loop generation");
		dclong.io.Console.print(proportion);
	}
	
	private static double[] sample(RandomDataImpl rng,double[] weight, int n){
		double[] proportion = new double[10];
		java.util.Arrays.fill(proportion, 0);
		for(int i=0; i<n; ++i){
			++proportion[dclong.stat.Random.sample(rng, weight)];
		}
		return dclong.util.Arrays.divide(proportion, n);
	}
	
	private static void initializeWeight(double[] weight){
		weight[0] = 1;
		weight[1] = 2;
		weight[2] = 3;
		weight[3] = 4;
		weight[4] = 1;
		weight[5] = 1;
		weight[6] = 1;
		weight[7] = 1;
		weight[8] = 1;
		weight[9] = 1;
	}
}
