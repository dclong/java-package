package dclong.util;

public class Timer {
	private long startingTime;
	private long endingTime;
	private long elapsedTime;
	
	public void start(){
		startingTime = System.currentTimeMillis();
	}
	
	public void begin(){
		start();
	}
	
	public void stop(){
		endingTime = System.currentTimeMillis();
		elapsedTime = endingTime - startingTime;
	}
	
	public void end(){
		stop();
	}
	
	public void printMilliseconds(String job){
		System.out.println("The time used for "+job+" is "+milliseconds()+" milliseconds.");
	}
	
	public void printSeconds(String job){
		System.out.println("The time used for "+job+" is "+seconds()+" seconds.");
	}
	
	public void printMinutes(String job){
		System.out.println("The time used for "+job+" is "+minutes()+" minutes.");
	}
	
	public void printHours(String job){
		System.out.println("The time used for "+job+" is "+hours()+" hours.");
	}
	
	public void printDays(String job){
		System.out.println("The time used for "+job+" is "+days()+" days.");
	}
	/**
	 * Get elapsed time in milliseconds.
	 * @return
	 */
	public long milliseconds(){
		return elapsedTime;
	}
	
	public double seconds(){
		return elapsedTime/1000d;
	}
	
	public double minutes(){
		return seconds()/60d;
	}
	
	public double hours(){
		return minutes()/60d;
	}
	
	public double days(){
		return hours()/24d;
	}
}
