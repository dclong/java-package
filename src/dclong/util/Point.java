package dclong.util;
/**
 * A class similar to java.awt.geom.Point2D.double with distance functionality and a 
 * static counter.
 * @author adu
 *
 */
public class Point {
	/**
	 * the x coordinate of the point
	 */
	private double x;
	/**
	 * the y coordinate of the point
	 */
	private double y;
	/**
	 * the id of the point
	 */
	private int id;
	/**
	 * the total number of points
	 */
	private static int count = 0;
	/**
	 * Construct a point with given location information. 
	 * The id of this point and the count of points are determined automatically.
	 * @param x
	 * @param y
	 */
	public Point(double x, double y){
		id = count;
		count++;
		this.x = x;
		this.y = y;
	}
	
	public Point(Point point){
		this(point.x,point.y);
	}
	/**
	 * Get the total number of points.
	 * @return the total number of points.
	 */
	public static int getCount(){
		return count;
	}
	/**
	 * Get the Euclidean distance between this point and other point.
	 * @param other another point. 
	 * @return the Euclidean distance between this point and other point.
	 */
	public double distance(Point other){
		double xDiff = other.x - x;
		double yDiff = other.y - y;
		return xDiff*xDiff + yDiff*yDiff;
	}
	
	/**
	 * Get the y coordinate of this point.
	 * @return the y coordinate of this point.
	 */
	public double getY(){
		return y;
	}
	/**
	 * Get the x coordinate of this point.
	 * @return the x coordinate of this point.
	 */
	public double getX(){
		return x;
	}
	/**
	 * Get the id of this point.
	 * @return the id of this point.
	 */
	public int getID(){
		return id;
	}
}
