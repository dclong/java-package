package dclong.io;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
/**
 * This class write a double array into a binary file.
 * This is primarily used as a way of exchanging binary data between Java and R. 
 * By calling an appropriate static method using package rJava in R, you can write a double/integer/boolean vector
 * into a binary file that's recognized by the DataInputStream class in Java. 
 * @author adu
 *
 */
public class BinaryWriter {
	//--------- One Dimensional Array -----------------------
	public static void writeDouble(double[] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int i=0; i<data.length; ++i){
			out.writeDouble(data[i]);
		}
		out.close();
	}
	
	public static void writeDouble(float[] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int i=0; i<data.length; ++i){
			out.writeDouble(data[i]);
		}
		out.close();
	}
	
	public static void writeDouble(int[] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int i=0; i<data.length; ++i){
			out.writeDouble(data[i]);
		}
		out.close();
	}
	
	public static void writeDouble(boolean[] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int i=0; i<data.length; ++i){
			out.writeDouble(data[i]?1:0);
		}
		out.close();
	}
	
	public static void writeFloat(double[] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int i=0; i<data.length; ++i){
			out.writeFloat((float)data[i]);
		}
		out.close();
	}
	
	public static void writeFloat(float[] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int i=0; i<data.length; ++i){
			out.writeFloat(data[i]);
		}
		out.close();
	}
	
	public static void writeFloat(int[] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int i=0; i<data.length; ++i){
			out.writeFloat(data[i]);
		}
		out.close();
	}
	
	public static void writeFloat(boolean[] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int i=0; i<data.length; ++i){
			out.writeFloat(data[i]?1:0);
		}
		out.close();
	}
	
	public static void writeInt(double[] data, String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int i=0; i<data.length; ++i){
			out.writeInt((int)data[i]);
		}
		out.close();
	}
	
	public static void writeInt(float[] data, String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int i=0; i<data.length; ++i){
			out.writeInt((int)data[i]);
		}
		out.close();
	}
	
	public static void writeInt(int[] data, String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int i=0; i<data.length; ++i){
			out.writeInt(data[i]);
		}
		out.close();
	}
	
	public static void writeInt(boolean[] data, String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int i=0; i<data.length; ++i){
			out.writeInt(data[i]?1:0);
		}
		out.close();
	}
	
	public static void writeBoolean(double[] data, String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int i=0; i<data.length; ++i){
			out.writeBoolean(data[i]!=0);
		}
		out.close();
	}
	
	public static void writeBoolean(float[] data, String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int i=0; i<data.length; ++i){
			out.writeBoolean(data[i]!=0);
		}
		out.close();
	}
	
	public static void writeBoolean(int[] data, String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int i=0; i<data.length; ++i){
			out.writeBoolean(data[i]!=0);
		}
		out.close();
	}
	
	public static void writeBoolean(boolean[] data, String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int i=0; i<data.length; ++i){
			out.writeBoolean(data[i]);
		}
		out.close();
	}
	
	//---------------- Two Dimensional Array ---------------------------
	public static void writeDouble(double[][] data,String outputFile, boolean byRow) throws IOException{
		if(byRow){
			writeDoubleByRow(data,outputFile);
			return;
		}
		writeDoubleByColumn(data,outputFile);
	}
	
	public static void writeDoubleByRow(double[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
			for(int columnIndex=0; columnIndex<data[rowIndex].length; ++columnIndex){
				out.writeDouble(data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeDoubleByColumn(double[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int columnIndex=0; columnIndex<data[0].length; ++columnIndex){
			for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
				out.writeDouble(data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeDouble(float[][] data,String outputFile, boolean byRow) throws IOException{
		if(byRow){
			writeDoubleByRow(data,outputFile);
			return;
		}
		writeDoubleByColumn(data,outputFile);
	}
	
	public static void writeDoubleByRow(float[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
			for(int columnIndex=0; columnIndex<data[rowIndex].length; ++columnIndex){
				out.writeDouble(data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeDoubleByColumn(float[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int columnIndex=0; columnIndex<data[0].length; ++columnIndex){
			for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
				out.writeDouble(data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeDouble(int[][] data,String outputFile, boolean byRow) throws IOException{
		if(byRow){
			writeDoubleByRow(data,outputFile);
			return;
		}
		writeDoubleByColumn(data,outputFile);
	}
	
	public static void writeDoubleByRow(int[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
			for(int columnIndex=0; columnIndex<data[rowIndex].length; ++columnIndex){
				out.writeDouble(data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeDoubleByColumn(int[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int columnIndex=0; columnIndex<data[0].length; ++columnIndex){
			for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
				out.writeDouble(data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeDouble(boolean[][] data,String outputFile, boolean byRow) throws IOException{
		if(byRow){
			writeDoubleByRow(data,outputFile);
			return;
		}
		writeDoubleByColumn(data,outputFile);
	}
	
	public static void writeDoubleByRow(boolean[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
			for(int columnIndex=0; columnIndex<data[rowIndex].length; ++columnIndex){
				out.writeDouble(data[rowIndex][columnIndex]?1:0);
			}
		}
		out.close();
	}
	
	public static void writeDoubleByColumn(boolean[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int columnIndex=0; columnIndex<data[0].length; ++columnIndex){
			for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
				out.writeDouble(data[rowIndex][columnIndex]?1:0);
			}
		}
		out.close();
	}
	
	public static void writeFloat(double[][] data,String outputFile, boolean byRow) throws IOException{
		if(byRow){
			writeFloatByRow(data,outputFile);
			return;
		}
		writeFloatByColumn(data,outputFile);
	}
	
	public static void writeFloatByRow(double[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
			for(int columnIndex=0; columnIndex<data[rowIndex].length; ++columnIndex){
				out.writeFloat((float)data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeFloatByColumn(double[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int columnIndex=0; columnIndex<data[0].length; ++columnIndex){
			for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
				out.writeFloat((float)data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeFloat(float[][] data,String outputFile, boolean byRow) throws IOException{
		if(byRow){
			writeFloatByRow(data,outputFile);
			return;
		}
		writeFloatByColumn(data,outputFile);
	}
	
	public static void writeFloatByRow(float[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
			for(int columnIndex=0; columnIndex<data[rowIndex].length; ++columnIndex){
				out.writeFloat(data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeFloatByColumn(float[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int columnIndex=0; columnIndex<data[0].length; ++columnIndex){
			for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
				out.writeFloat(data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeFloat(int[][] data,String outputFile, boolean byRow) throws IOException{
		if(byRow){
			writeFloatByRow(data,outputFile);
			return;
		}
		writeFloatByColumn(data,outputFile);
	}
	
	public static void writeFloatByRow(int[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
			for(int columnIndex=0; columnIndex<data[rowIndex].length; ++columnIndex){
				out.writeFloat(data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeFloatByColumn(int[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int columnIndex=0; columnIndex<data[0].length; ++columnIndex){
			for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
				out.writeFloat(data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeFloat(boolean[][] data,String outputFile, boolean byRow) throws IOException{
		if(byRow){
			writeFloatByRow(data,outputFile);
			return;
		}
		writeFloatByColumn(data,outputFile);
	}
	
	public static void writeFloatByRow(boolean[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
			for(int columnIndex=0; columnIndex<data[rowIndex].length; ++columnIndex){
				out.writeFloat(data[rowIndex][columnIndex]?1:0);
			}
		}
		out.close();
	}
	
	public static void writeFloatByColumn(boolean[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int columnIndex=0; columnIndex<data[0].length; ++columnIndex){
			for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
				out.writeFloat(data[rowIndex][columnIndex]?1:0);
			}
		}
		out.close();
	}
	
	public static void writeInt(double[][] data,String outputFile, boolean byRow) throws IOException{
		if(byRow){
			writeIntByRow(data,outputFile);
			return;
		}
		writeIntByColumn(data,outputFile);
	}
	
	public static void writeIntByRow(double[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
			for(int columnIndex=0; columnIndex<data[rowIndex].length; ++columnIndex){
				out.writeInt((int)data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeIntByColumn(double[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int columnIndex=0; columnIndex<data[0].length; ++columnIndex){
			for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
				out.writeInt((int)data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeInt(float[][] data,String outputFile, boolean byRow) throws IOException{
		if(byRow){
			writeIntByRow(data,outputFile);
			return;
		}
		writeIntByColumn(data,outputFile);
	}
	
	public static void writeIntByRow(float[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
			for(int columnIndex=0; columnIndex<data[rowIndex].length; ++columnIndex){
				out.writeInt((int)data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeIntByColumn(float[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int columnIndex=0; columnIndex<data[0].length; ++columnIndex){
			for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
				out.writeInt((int)data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeInt(int[][] data,String outputFile, boolean byRow) throws IOException{
		if(byRow){
			writeIntByRow(data,outputFile);
			return;
		}
		writeIntByColumn(data,outputFile);
	}
	
	public static void writeIntByRow(int[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
			for(int columnIndex=0; columnIndex<data[rowIndex].length; ++columnIndex){
				out.writeInt(data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeIntByColumn(int[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int columnIndex=0; columnIndex<data[0].length; ++columnIndex){
			for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
				out.writeInt(data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeInt(boolean[][] data,String outputFile, boolean byRow) throws IOException{
		if(byRow){
			writeIntByRow(data,outputFile);
			return;
		}
		writeIntByColumn(data,outputFile);
	}
	
	public static void writeIntByRow(boolean[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
			for(int columnIndex=0; columnIndex<data[rowIndex].length; ++columnIndex){
				out.writeInt(data[rowIndex][columnIndex]?1:0);
			}
		}
		out.close();
	}
	
	public static void writeIntByColumn(boolean[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int columnIndex=0; columnIndex<data[0].length; ++columnIndex){
			for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
				out.writeInt(data[rowIndex][columnIndex]?1:0);
			}
		}
		out.close();
	}
	
	public static void writeBoolean(double[][] data,String outputFile, boolean byRow) throws IOException{
		if(byRow){
			writeBooleanByRow(data,outputFile);
			return;
		}
		writeBooleanByColumn(data,outputFile);
	}
	
	public static void writeBooleanByRow(double[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
			for(int columnIndex=0; columnIndex<data[rowIndex].length; ++columnIndex){
				out.writeBoolean(data[rowIndex][columnIndex]!=0);
			}
		}
		out.close();
	}
	
	public static void writeBooleanByColumn(double[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int columnIndex=0; columnIndex<data[0].length; ++columnIndex){
			for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
				out.writeBoolean(data[rowIndex][columnIndex]!=0);
			}
		}
		out.close();
	}
	
	public static void writeBoolean(float[][] data,String outputFile, boolean byRow) throws IOException{
		if(byRow){
			writeBooleanByRow(data,outputFile);
			return;
		}
		writeBooleanByColumn(data,outputFile);
	}
	
	public static void writeBooleanByRow(float[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
			for(int columnIndex=0; columnIndex<data[rowIndex].length; ++columnIndex){
				out.writeBoolean(data[rowIndex][columnIndex]!=0);
			}
		}
		out.close();
	}
	
	public static void writeBooleanByColumn(float[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int columnIndex=0; columnIndex<data[0].length; ++columnIndex){
			for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
				out.writeBoolean(data[rowIndex][columnIndex]!=0);
			}
		}
		out.close();
	}
	
	public static void writeBoolean(int[][] data,String outputFile, boolean byRow) throws IOException{
		if(byRow){
			writeBooleanByRow(data,outputFile);
			return;
		}
		writeBooleanByColumn(data,outputFile);
	}
	
	public static void writeBooleanByRow(int[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
			for(int columnIndex=0; columnIndex<data[rowIndex].length; ++columnIndex){
				out.writeBoolean(data[rowIndex][columnIndex]!=0);
			}
		}
		out.close();
	}
	
	public static void writeBooleanByColumn(int[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int columnIndex=0; columnIndex<data[0].length; ++columnIndex){
			for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
				out.writeBoolean(data[rowIndex][columnIndex]!=0);
			}
		}
		out.close();
	}
	
	public static void writeBoolean(boolean[][] data,String outputFile, boolean byRow) throws IOException{
		if(byRow){
			writeBooleanByRow(data,outputFile);
			return;
		}
		writeBooleanByColumn(data,outputFile);
	}
	
	public static void writeBooleanByRow(boolean[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
			for(int columnIndex=0; columnIndex<data[rowIndex].length; ++columnIndex){
				out.writeBoolean(data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	
	public static void writeBooleanByColumn(boolean[][] data,String outputFile) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
		for(int columnIndex=0; columnIndex<data[0].length; ++columnIndex){
			for(int rowIndex=0; rowIndex<data.length; ++rowIndex){
				out.writeBoolean(data[rowIndex][columnIndex]);
			}
		}
		out.close();
	}
	//---------------- Three Dimensional Array ---------------------------
		public static void writeDouble(double[][][] data,String outputFile) throws IOException{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
			for(int i=0; i<data.length; ++i){
				for(int j=0; j<data[i].length; ++j){
					for(int k=0; k<data[i][j].length; ++k)
					out.writeDouble(data[i][j][k]);
				}
			}
			out.close();
		}
		
		public static void writeDouble(float[][][] data,String outputFile) throws IOException{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
			for(int i=0; i<data.length; ++i){
				for(int j=0; j<data[i].length; ++j){
					for(int k=0; k<data[i][j].length; ++k)
					out.writeDouble(data[i][j][k]);
				}
			}
			out.close();
		}
		
		public static void writeDouble(int[][][] data,String outputFile) throws IOException{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
			for(int i=0; i<data.length; ++i){
				for(int j=0; j<data[i].length; ++j){
					for(int k=0; k<data[i][j].length; ++k)
					out.writeDouble(data[i][j][k]);
				}
			}
			out.close();
		}
		
		public static void writeDouble(boolean[][][] data,String outputFile) throws IOException{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
			for(int i=0; i<data.length; ++i){
				for(int j=0; j<data[i].length; ++j){
					for(int k=0; k<data[i][j].length; ++k)
					out.writeDouble(data[i][j][k]?1:0);
				}
			}
			out.close();
		}
		
		public static void writeFloat(double[][][] data,String outputFile) throws IOException{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
			for(int i=0; i<data.length; ++i){
				for(int j=0; j<data[i].length; ++j){
					for(int k=0; k<data[i][j].length; ++k)
					out.writeFloat((float)data[i][j][k]);
				}
			}
			out.close();
		}
		
		public static void writeFloat(float[][][] data,String outputFile) throws IOException{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
			for(int i=0; i<data.length; ++i){
				for(int j=0; j<data[i].length; ++j){
					for(int k=0; k<data[i][j].length; ++k)
					out.writeFloat(data[i][j][k]);
				}
			}
			out.close();
		}
		
		public static void writeFloat(int[][][] data,String outputFile) throws IOException{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
			for(int i=0; i<data.length; ++i){
				for(int j=0; j<data[i].length; ++j){
					for(int k=0; k<data[i][j].length; ++k)
					out.writeFloat(data[i][j][k]);
				}
			}
			out.close();
		}
		
		public static void writeFloat(boolean[][][] data,String outputFile) throws IOException{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
			for(int i=0; i<data.length; ++i){
				for(int j=0; j<data[i].length; ++j){
					for(int k=0; k<data[i][j].length; ++k)
					out.writeFloat(data[i][j][k]?1:0);
				}
			}
			out.close();
		}
		
		public static void writeInt(double[][][] data,String outputFile) throws IOException{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
			for(int i=0; i<data.length; ++i){
				for(int j=0; j<data[i].length; ++j){
					for(int k=0; k<data[i][j].length; ++k)
					out.writeInt((int)data[i][j][k]);
				}
			}
			out.close();
		}
		
		public static void writeInt(float[][][] data,String outputFile) throws IOException{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
			for(int i=0; i<data.length; ++i){
				for(int j=0; j<data[i].length; ++j){
					for(int k=0; k<data[i][j].length; ++k)
					out.writeInt((int)data[i][j][k]);
				}
			}
			out.close();
		}
		
		public static void writeInt(int[][][] data,String outputFile) throws IOException{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
			for(int i=0; i<data.length; ++i){
				for(int j=0; j<data[i].length; ++j){
					for(int k=0; k<data[i][j].length; ++k)
					out.writeInt(data[i][j][k]);
				}
			}
			out.close();
		}
		
		public static void writeInt(boolean[][][] data,String outputFile) throws IOException{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
			for(int i=0; i<data.length; ++i){
				for(int j=0; j<data[i].length; ++j){
					for(int k=0; k<data[i][j].length; ++k)
					out.writeInt(data[i][j][k]?1:0);
				}
			}
			out.close();
		}
		
		public static void writeBoolean(double[][][] data,String outputFile) throws IOException{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
			for(int i=0; i<data.length; ++i){
				for(int j=0; j<data[i].length; ++j){
					for(int k=0; k<data[i][j].length; ++k)
					out.writeBoolean(data[i][j][k]!=0);
				}
			}
			out.close();
		}
		
		public static void writeBoolean(float[][][] data,String outputFile) throws IOException{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
			for(int i=0; i<data.length; ++i){
				for(int j=0; j<data[i].length; ++j){
					for(int k=0; k<data[i][j].length; ++k)
					out.writeBoolean(data[i][j][k]!=0);
				}
			}
			out.close();
		}
		
		public static void writeBoolean(int[][][] data,String outputFile) throws IOException{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
			for(int i=0; i<data.length; ++i){
				for(int j=0; j<data[i].length; ++j){
					for(int k=0; k<data[i][j].length; ++k)
					out.writeBoolean(data[i][j][k]!=0);
				}
			}
			out.close();
		}
		
		public static void writeBoolean(boolean[][][] data,String outputFile) throws IOException{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));
			for(int i=0; i<data.length; ++i){
				for(int j=0; j<data[i].length; ++j){
					for(int k=0; k<data[i][j].length; ++k)
					out.writeBoolean(data[i][j][k]);
				}
			}
			out.close();
		}
}
