package dclong.io;

public class Console {
	
	public static void print(Object[] array, String delimiter, int from, int length){
		for(int i=Math.max(0, from); i<Math.min(from+length, array.length); ++i){
			System.out.print(array[i] + delimiter);
		}
	}
	
	public static void print(Object[] array) {
		print(array, " ");
	}
	
	public static void print(Object[] array, String delimiter) {
		print(array, delimiter, 0, array.length);
	}
	
	public static void print(Object[] array, int from, int length) {
		print(array, " ", from, length);
	}
	
	public static void print(char[] array, String delimiter, int from, int length){
		for(int i=Math.max(0, from); i<Math.min(from+length, array.length); ++i){
			System.out.print(array[i] + delimiter);
		}
	}
	
	public static void print(char[] array) {
		print(array, " ");
	}
	
	public static void print(char[] array, String delimiter) {
		print(array, delimiter, 0, array.length);
	}
	
	public static void print(char[] array, int from, int length) {
		print(array, " ", from, length);
	}
	/**
	 * Print out an integer array.
	 * 
	 * @param array
	 *            the array to be printed out.
	 * @param delimiter
	 *            the delimiter to separate elements of the array.
	 * @param from
	 *            the place starting from where the array is printed out.
	 * @param length
	 *            the number of elements to be printed out.
	 */
	public static void print(int[] array, String delimiter, int from,
			int length) {
		for (int i = Math.max(0, from); i < Math.min(from + length,
				array.length); ++i) {
			System.out.print(array[i] + delimiter);
		}
	}

	/**
	 * Print out an integer array.
	 * 
	 * @param array
	 *            the array to be printed out.
	 * @param delimiter
	 *            the delimiter to separate elements of the array.
	 */
	public static void print(int[] array, String delimiter) {
		print(array, delimiter, 0, array.length);
	}

	/**
	 * Print out an integer array with elements separated by a space.
	 * 
	 * @param array
	 *            the array to be printed out.
	 */
	public static void print(int[] array) {
		print(array, " ");
	}

	/**
	 * Print out an integer array with elements separated by a space.
	 * 
	 * @param array
	 *            the array to be printed out.
	 * @param from
	 *            the place starting from where the array is printed out.
	 * @param length
	 *            the number of elements to be printed out.
	 */
	public static void print(int[] array, int from, int length) {
		print(array, " ", from, length);
	}

	/**
	 * Print out an boolean array.
	 * 
	 * @param array
	 *            the array to be printed out.
	 * @param delimiter
	 *            the delimiter to separate elements of the array.
	 * @param from
	 *            the place starting from where the array is printed out.
	 * @param length
	 *            the number of elements to be printed out.
	 */
	public static void print(boolean[] array, String delimiter, int from,
			int length) {
		for (int i = Math.max(0, from); i < Math.min(from + length,
				array.length); ++i) {
			System.out.print(array[i] + delimiter);
		}
	}
	/**
	 * Print out an boolean array.
	 * 
	 * @param array
	 *            the array to be printed out.
	 * @param delimiter
	 *            the delimiter to separate elements of the array.
	 */
	public static void print(boolean[] array, String delimiter) {
		print(array, delimiter, 0, array.length);
	}

	/**
	 * Print out an boolean array with elements separated by a space.
	 * 
	 * @param array
	 *            the array to be printed out.
	 */
	public static void print(boolean[] array) {
		print(array, " ");
	}

	/**
	 * Print out an boolean array with elements separated by a space.
	 * 
	 * @param array
	 *            the array to be printed out.
	 * @param from
	 *            the place starting from where the array is printed out.
	 * @param length
	 *            the number of elements to be printed out.
	 */
	public static void print(boolean[] array, int from, int length) {
		print(array, " ", from, length);
	}

	/**
	 * Print out an double array.
	 * 
	 * @param array
	 *            the array to be printed out.
	 * @param delimiter
	 *            the delimiter to separate elements of the array.
	 * @param from
	 *            the place starting from where the array is printed out.
	 * @param length
	 *            the number of elements to be printed out.
	 */
	public static void print(double[] array, String delimiter, int from,
			int length) {
		for (int i = Math.max(0, from); i < Math.min(from + length,
				array.length); ++i) {
			System.out.print(array[i] + delimiter);
		}
	}

	/**
	 * Print out an double array.
	 * 
	 * @param array
	 *            the array to be printed out.
	 * @param delimiter
	 *            the delimiter to separate elements of the array.
	 */
	public static void print(double[] array, String delimiter) {
		print(array, delimiter, 0, array.length);
	}

	/**
	 * Print out an double array with elements separated by a space.
	 * 
	 * @param array
	 *            the array to be printed out.
	 */
	public static void print(double[] array) {
		print(array, " ");
	}

	/**
	 * Print out an double array with elements separated by a space.
	 * 
	 * @param array
	 *            the array to be printed out.
	 * @param from
	 *            the place starting from where the array is printed out.
	 * @param length
	 *            the number of elements to be printed out.
	 */
	public static void print(double[] array, int from, int length) {
		print(array, " ", from, length);
	}

	/**
	 * Print out an float array.
	 * 
	 * @param array
	 *            the array to be printed out.
	 * @param delimiter
	 *            the delimiter to separate elements of the array.
	 * @param from
	 *            the place starting from where the array is printed out.
	 * @param length
	 *            the number of elements to be printed out.
	 */
	public static void print(float[] array, String delimiter, int from,
			int length) {
		for (int i = Math.max(0, from); i < Math.min(from + length,
				array.length); ++i) {
			System.out.print(array[i] + delimiter);
		}
	}

	/**
	 * Print out an float array.
	 * 
	 * @param array
	 *            the array to be printed out.
	 * @param delimiter
	 *            the delimiter to separate elements of the array.
	 */
	public static void print(float[] array, String delimiter) {
		print(array, delimiter, 0, array.length);
	}

	/**
	 * Print out an float array with elements separated by a space.
	 * 
	 * @param array
	 *            the array to be printed out.
	 */
	public static void print(float[] array) {
		print(array, " ");
	}

	/**
	 * Print out an float array with elements separated by a space.
	 * 
	 * @param array
	 *            the array to be printed out.
	 * @param from
	 *            the place starting from where the array is printed out.
	 * @param length
	 *            the number of elements to be printed out.
	 */
	public static void print(float[] array, int from, int length) {
		print(array, " ", from, length);
	}

	/**
	 * Print out an String array.
	 * 
	 * @param array
	 *            the array to be printed out.
	 * @param delimiter
	 *            the delimiter to separate elements of the array.
	 * @param from
	 *            the place starting from where the array is printed out.
	 * @param length
	 *            the number of elements to be printed out.
	 */
	public static void print(String[] array, String delimiter, int from,
			int length) {
		for (int i = Math.max(0, from); i < Math.min(from + length,
				array.length); ++i) {
			System.out.print(array[i] + delimiter);
		}
	}

	/**
	 * Print out an String array.
	 * 
	 * @param array
	 *            the array to be printed out.
	 * @param delimiter
	 *            the delimiter to separate elements of the array.
	 */
	public static void print(String[] array, String delimiter) {
		print(array, delimiter, 0, array.length);
	}

	/**
	 * Print out an String array with elements separated by a space.
	 * 
	 * @param array
	 *            the array to be printed out.
	 */
	public static void print(String[] array) {
		print(array, " ");
	}

	/**
	 * Print out an String array with elements separated by a space.
	 * 
	 * @param array
	 *            the array to be printed out.
	 * @param from
	 *            the place starting from where the array is printed out.
	 * @param length
	 *            the number of elements to be printed out.
	 */
	public static void print(String[] array, int from, int length) {
		print(array, " ", from, length);
	}

	// -------------- Two Dimensional Array ---------------
	/**
	 * Print out a 2-d array.
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 * @param columnDelimiter
	 *            the delimiter separating elements in the same row.
	 * @param rowDelimiter
	 *            the delimiter separating different rows.
	 * @param rowFrom
	 *            the row index from where the array is printed out.
	 * @param rowNumber
	 *            the number of rows to be printed out.
	 * @param columnFrom
	 *            the column index from where the array is printed out.
	 * @param columnNumber
	 *            the number of columns to be printed out.
	 */
	public static void print(float[][] array, String columnDelimiter,
			String rowDelimiter, int rowFrom, int rowNumber, int columnFrom,
			int columnNumber) {
		for (int rowIndex = Math.max(0, rowFrom); rowIndex < Math.min(rowFrom
				+ rowNumber, array.length); ++rowIndex) {
			for (int columnIndex = Math.max(0, columnFrom); columnIndex < Math
					.min(columnFrom + columnNumber, array[rowIndex].length); ++columnIndex) {
				System.out
						.print(array[rowIndex][columnIndex] + columnDelimiter);
			}
			System.out.print(rowDelimiter);
		}
	}

	/**
	 * Print out a 2-d array with elements in the same row separated by a space
	 * and different row separated by "\n".
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 * @param rowFrom
	 *            the row index from where the array is printed out.
	 * @param rowLength
	 *            the number of rows to be printed out.
	 * @param columnFrom
	 *            the column index from where the array is printed out.
	 * @param columnLength
	 *            the number of columns to be printed out.
	 */
	public static void print(float[][] array, int rowFrom, int rowLength,
			int columnFrom, int columnLength) {
		print(array, " ", "\n", rowFrom, rowLength, columnFrom,
				columnLength);
	}

	/**
	 * Print out a 2-d array.
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 * @param columnDelimiter
	 *            the delimiter separating elements in the same row.
	 * @param rowDelimiter
	 *            the delimiter separating different rows.
	 */
	public static void print(float[][] array, String columnDelimiter,
			String rowDelimiter) {
		for (int rowIndex = 0; rowIndex < array.length; ++rowIndex) {
			for (int columnIndex = 0; columnIndex < array[rowIndex].length; ++columnIndex) {
				System.out
						.print(array[rowIndex][columnIndex] + columnDelimiter);
			}
			System.out.print(rowDelimiter);
		}
	}

	/**
	 * Print out a 2-d array with elements in the same row separated by a space
	 * and different rows separated by "\n".
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 */
	public static void print(float[][] array) {
		print(array, " ", "\n");
	}

	/**
	 * Print out a 2-d array.
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 * @param columnDelimiter
	 *            the delimiter separating elements in the same row.
	 * @param rowDelimiter
	 *            the delimiter separating different rows.
	 * @param rowFrom
	 *            the row index from where the array is printed out.
	 * @param rowLength
	 *            the number of rows to be printed out.
	 * @param columnFrom
	 *            the column index from where the array is printed out.
	 * @param columnLength
	 *            the number of columns to be printed out.
	 */
	public static void print(String[][] array, String columnDelimiter,
			String rowDelimiter, int rowFrom, int rowLength, int columnFrom,
			int columnLength) {
		for (int rowIndex = Math.max(0, rowFrom); rowIndex < Math.min(rowFrom
				+ rowLength, array.length); ++rowIndex) {
			for (int columnIndex = Math.max(0, columnFrom); columnIndex < Math
					.min(columnFrom + columnLength, array[rowIndex].length); ++columnIndex) {
				System.out
						.print(array[rowIndex][columnIndex] + columnDelimiter);
			}
			System.out.print(rowDelimiter);
		}
	}

	/**
	 * Print out a 2-d array with elements in the same row separated by a space
	 * and different rows separated by "\n".
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 * @param rowFrom
	 *            the row index from where the array is printed out.
	 * @param rowLength
	 *            the number of rows to be printed out.
	 * @param columnFrom
	 *            the column index from where the array is printed out.
	 * @param columnLength
	 *            the number of columns to be printed out.
	 */
	public static void print(String[][] array, int rowFrom, int rowLength,
			int columnFrom, int columnLength) {
		print(array, " ", "\n", rowFrom, rowLength, columnFrom,
				columnLength);
	}

	/**
	 * Print out a 2-d array.
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 * @param columnDelimiter
	 *            the delimiter separating elements in the same row.
	 * @param rowDelimiter
	 *            the delimiter separating different rows.
	 */
	public static void print(String[][] array, String columnDelimiter,
			String rowDelimiter) {
		for (int rowIndex = 0; rowIndex < array.length; ++rowIndex) {
			for (int columnIndex = 0; columnIndex < array[rowIndex].length; ++columnIndex) {
				System.out
						.print(array[rowIndex][columnIndex] + columnDelimiter);
			}
			System.out.print(rowDelimiter);
		}
	}

	/**
	 * Print out a 2-d array with elements in the same row separated by a space
	 * and different rows separated by "\n".
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 */
	public static void print(String[][] array) {
		print(array, " ", "\n");
	}

	/**
	 * Print out a 2-d array.
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 * @param columnDelimiter
	 *            the delimiter separating elements in the same row.
	 * @param rowDelimiter
	 *            the delimiter separating different rows.
	 * @param rowFrom
	 *            the row index from where the array is printed out.
	 * @param rowNumber
	 *            the number of rows to be printed out.
	 * @param columnFrom
	 *            the column index from where the array is printed out.
	 * @param columnNumber
	 *            the number of columns to be printed out.
	 */
	public static void print(double[][] array, String columnDelimiter,
			String rowDelimiter, int rowFrom, int rowNumber, int columnFrom,
			int columnNumber) {
		for (int rowIndex = Math.max(0, rowFrom); rowIndex < Math.min(rowFrom
				+ rowNumber, array.length); ++rowIndex) {
			for (int columnIndex = Math.max(0, columnFrom); columnIndex < Math
					.min(columnFrom + columnNumber, array[rowIndex].length); ++columnIndex) {
				System.out
						.print(array[rowIndex][columnIndex] + columnDelimiter);
			}
			System.out.print(rowDelimiter);
		}
	}

	/**
	 * Print out a 2-d array with elements in the same row separated by a space
	 * and different rows separated by "\n".
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 * @param rowFrom
	 *            the row index from where the array is printed out.
	 * @param rowLength
	 *            the number of rows to be printed out.
	 * @param columnFrom
	 *            the column index from where the array is printed out.
	 * @param columnLength
	 *            the number of columns to be printed out.
	 */
	public static void print(double[][] array, int rowFrom, int rowLength,
			int columnFrom, int columnLength) {
		print(array, " ", "\n", rowFrom, rowLength, columnFrom,
				columnLength);
	}

	/**
	 * Print out a 2-d array.
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 * @param columnDelimiter
	 *            the delimiter separating elements in the same row.
	 * @param rowDelimiter
	 *            the delimiter separating different rows.
	 */
	public static void print(double[][] array, String columnDelimiter,
			String rowDelimiter) {
		for (int rowIndex = 0; rowIndex < array.length; ++rowIndex) {
			for (int columnIndex = 0; columnIndex < array[rowIndex].length; ++columnIndex) {
				System.out
						.print(array[rowIndex][columnIndex] + columnDelimiter);
			}
			System.out.print(rowDelimiter);
		}
	}

	/**
	 * Print out a 2-d array with elements in the same row separated by a space
	 * and different rows separated by "\n".
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 */
	public static void print(double[][] array) {
		print(array, " ", "\n");
	}
	
	public static void print(Object[][] array, String columnDelimiter,
			String rowDelimiter, int rowFrom, int rowLength, int columnFrom,
			int columnLength) {
		for (int rowIndex = Math.max(0, rowFrom); rowIndex < Math.min(rowFrom
				+ rowLength, array.length); ++rowIndex) {
			for (int columnIndex = Math.max(0, columnFrom); columnIndex < Math
					.min(columnFrom + columnLength, array[rowIndex].length); ++columnIndex) {
				System.out
						.print(array[rowIndex][columnIndex] + columnDelimiter);
			}
			System.out.print(rowDelimiter);
		}
	}
	
	public static void print(Object[][] array, int rowFrom, int rowLength,
			int columnFrom, int columnLength) {
		print(array, " ", "\n", rowFrom, rowLength, columnFrom,
				columnLength);
	}
	/**
	 * Print out a 2-d array.
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 * @param columnDelimiter
	 *            the delimiter separating elements in the same row.
	 * @param rowDelimiter
	 *            the delimiter separating different rows.
	 * @param rowFrom
	 *            the row index from where the array is printed out.
	 * @param rowLength
	 *            the number of rows to be printed out.
	 * @param columnFrom
	 *            the column index from where the array is printed out.
	 * @param columnLength
	 *            the number of columns to be printed out.
	 */
	public static void print(int[][] array, String columnDelimiter,
			String rowDelimiter, int rowFrom, int rowLength, int columnFrom,
			int columnLength) {
		for (int rowIndex = Math.max(0, rowFrom); rowIndex < Math.min(rowFrom
				+ rowLength, array.length); ++rowIndex) {
			for (int columnIndex = Math.max(0, columnFrom); columnIndex < Math
					.min(columnFrom + columnLength, array[rowIndex].length); ++columnIndex) {
				System.out
						.print(array[rowIndex][columnIndex] + columnDelimiter);
			}
			System.out.print(rowDelimiter);
		}
	}
	
	public static void print(Object[][] array, String columnDelimiter,
			String rowDelimiter) {
		for (int rowIndex = 0; rowIndex < array.length; ++rowIndex) {
			for (int columnIndex = 0; columnIndex < array[rowIndex].length; ++columnIndex) {
				System.out
						.print(array[rowIndex][columnIndex] + columnDelimiter);
			}
			System.out.print(rowDelimiter);
		}
	}

	public static void print(Object[][] array) {
		print(array, " ", "\n");
	}
	/**
	 * Print out a 2-d array with elements in the same row separated by a space
	 * and different rows separated by "\n".
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 * @param rowFrom
	 *            the row index from where the array is printed out.
	 * @param rowLength
	 *            the number of rows to be printed out.
	 * @param columnFrom
	 *            the column index from where the array is printed out.
	 * @param columnLength
	 *            the number of columns to be printed out.
	 */
	public static void print(int[][] array, int rowFrom, int rowLength,
			int columnFrom, int columnLength) {
		print(array, " ", "\n", rowFrom, rowLength, columnFrom,
				columnLength);
	}

	/**
	 * Print out a 2-d array.
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 * @param columnDelimiter
	 *            the delimiter separating elements in the same row.
	 * @param rowDelimiter
	 *            the delimiter separating different rows.
	 */
	public static void print(int[][] array, String columnDelimiter,
			String rowDelimiter) {
		for (int rowIndex = 0; rowIndex < array.length; ++rowIndex) {
			for (int columnIndex = 0; columnIndex < array[rowIndex].length; ++columnIndex) {
				System.out
						.print(array[rowIndex][columnIndex] + columnDelimiter);
			}
			System.out.print(rowDelimiter);
		}
	}

	/**
	 * Print out a 2-d array with elements in the same row separated by a space
	 * and different rows separated by "\n".
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 */
	public static void print(int[][] array) {
		print(array, " ", "\n");
	}

	public static void print(char[][] array, String columnDelimiter,
			String rowDelimiter) {
		for (int rowIndex = 0; rowIndex < array.length; ++rowIndex) {
			for (int columnIndex = 0; columnIndex < array[rowIndex].length; ++columnIndex) {
				System.out
						.print(array[rowIndex][columnIndex] + columnDelimiter);
			}
			System.out.print(rowDelimiter);
		}
	}
	
	public static void print(char[][] array, int rowFrom, int rowLength,
			int columnFrom, int columnLength) {
		print(array, " ", "\n", rowFrom, rowLength, columnFrom,
				columnLength);
	}
	
	public static void print(char[][] array, String columnDelimiter,
			String rowDelimiter, int rowFrom, int rowLength, int columnFrom,
			int columnLength) {
		for (int rowIndex = Math.max(0, rowFrom); rowIndex < Math.min(rowFrom
				+ rowLength, array.length); ++rowIndex) {
			for (int columnIndex = Math.max(0, columnFrom); columnIndex < Math
					.min(columnFrom + columnLength, array[rowIndex].length); ++columnIndex) {
				System.out
						.print(array[rowIndex][columnIndex] + columnDelimiter);
			}
			System.out.print(rowDelimiter);
		}
	}
	
	public static void print(char[][] array) {
		print(array, " ", "\n");
	}
	/**
	 * Print out a 2-d array.
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 * @param columnDelimiter
	 *            the delimiter separating elements in the same row.
	 * @param rowDelimiter
	 *            the delimiter separating different rows.
	 * @param rowFrom
	 *            the row index from where the array is printed out.
	 * @param rowLength
	 *            the number of rows to be printed out.
	 * @param columnFrom
	 *            the column index from where the array is printed out.
	 * @param columnLength
	 *            the number of columns to be printed out.
	 */
	public static void print(boolean[][] array, String columnDelimiter,
			String rowDelimiter, int rowFrom, int rowLength, int columnFrom,
			int columnLength) {
		for (int rowIndex = Math.max(0, rowFrom); rowIndex < Math.min(rowFrom
				+ rowLength, array.length); ++rowIndex) {
			for (int columnIndex = Math.max(0, columnFrom); columnIndex < Math
					.min(columnFrom + columnLength, array[rowIndex].length); ++columnIndex) {
				System.out
						.print(array[rowIndex][columnIndex] + columnDelimiter);
			}
			System.out.print(rowDelimiter);
		}
	}

	/**
	 * Print out a 2-d array with elements in the same row separated by a space
	 * and different rows separated by "\n".
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 * @param rowFrom
	 *            the row index from where the array is printed out.
	 * @param rowLength
	 *            the number of rows to be printed out.
	 * @param columnFrom
	 *            the column index from where the array is printed out.
	 * @param columnLength
	 *            the number of columns to be printed out.
	 */
	public static void print(boolean[][] array, int rowFrom,
			int rowLength, int columnFrom, int columnLength) {
		print(array, " ", "\n", rowFrom, rowLength, columnFrom,
				columnLength);
	}

	/**
	 * Print out a 2-d array.
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 * @param columnDelimiter
	 *            the delimiter separating elements in the same row.
	 * @param rowDelimiter
	 *            the delimiter separating different rows.
	 */
	public static void print(boolean[][] array, String columnDelimiter,
			String rowDelimiter) {
		for (int rowIndex = 0; rowIndex < array.length; ++rowIndex) {
			for (int columnIndex = 0; columnIndex < array[rowIndex].length; ++columnIndex) {
				System.out
						.print(array[rowIndex][columnIndex] + columnDelimiter);
			}
			System.out.print(rowDelimiter);
		}
	}

	/**
	 * Print out a 2-d array with elements in the same row separated by a space
	 * and different rows separated by "\n".
	 * 
	 * @param array
	 *            the 2-d array to be printed out.
	 */
	public static void print(boolean[][] array) {
		print(array, " ", "\n");
	}
}
