package dclong.io;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class BinaryReader {
	/**
	 * Read binary data from a file into a double array. 
	 * @param file a file from which data is to be read in.
	 * @param first the 0-based byte position for the first data to be read in.
	 * @param skipBytes the number of bytes to skip regularly in the reading process.
	 * Notice that when a data is read in, the file pointer is at the end of the data, so 
	 * be careful not to count bytes for the data that has already been read in.
	 * @param length the total number of data to read in.
	 * @return a double array. 
	 * @throws IOException
	 */
	public static double[] readDouble(String file, int first, int skipBytes, int length) throws IOException{
		double[] data = new double[length];
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		//find the first double to read in
		in.skipBytes(first);
		data[0] = in.readDouble();
		for(int i=1; i<length; ++i){
			in.skipBytes(skipBytes);
			data[i] = in.readDouble();
		}
		in.close();
		return data;
	}
	
	public static float[] readFloat(String file, int first, int skipBytes, int length) throws IOException{
		float[] data = new float[length];
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		//find the first double to read in
		in.skipBytes(first);
		data[0] = in.readFloat();
		for(int i=1; i<length; ++i){
			in.skipBytes(skipBytes);
			data[i] = in.readFloat();
		}
		in.close();
		return data;
	}
	
	/**
	 * Read binary data from a file into a integer array. 
	 * @param file a file from which data is to be read in.
	 * @param first the 0-based byte position for the first data to be read in.
	 * @param skipBytes the number of bytes to skip regularly in the reading process.
	 * Notice that when a data is read in, the file pointer is at the end of the data, so 
	 * be careful not to count bytes for the data that has already been read in.
	 * @param length the total number of data to read in.
	 * @return a double array. 
	 * @throws IOException
	 */
	public static int[] readInt(String file, int first, int skipBytes, int length) throws IOException{
		int[] data = new int[length];
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		//find the first double to read in
		in.skipBytes(first);
		data[0] = in.readInt();
		for(int i=1; i<length; ++i){
			in.skipBytes(skipBytes);
			data[i] = in.readInt();
		}
		in.close();
		return data;
	}

	public static boolean[] readBoolean(String file, int first, int skipBytes, int length) throws IOException{
		boolean[] data = new boolean[length];
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		//find the first double to read in
		in.skipBytes(first);
		data[0] = in.readBoolean();
		for(int i=1; i<length; ++i){
			in.skipBytes(skipBytes);
			data[i] = in.readBoolean();
		}
		in.close();
		return data;
	}

	/**
	 * Read all data from a file into a double array. 
	 * @param file a file from which data is read in.
	 * @return a 1-d double array.
	 * @throws IOException
	 */
	public static double[] readDouble(String file) throws IOException{
		ArrayList<Double> data = new ArrayList<Double>(1000);
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		while(in.available()>0){
			data.add(in.readDouble());
		}
		in.close();
		double[] doubleData = new double[data.size()];
		for(int i=0; i<doubleData.length; ++i){
			doubleData[i] = data.get(i);
		}
		return doubleData;
	} 
	/**
	 * Read all data from a file into an integer array. 
	 * @param file a file from which data is read in.
	 * @return a 1-d integer array.
	 * @throws IOException
	 */
	public static int[] readInt(String file) throws IOException{
		ArrayList<Integer> data = new ArrayList<Integer>(1000);
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		while(in.available()>0){
			data.add(in.readInt());
		}
		in.close();
		int[] intData = new int[data.size()];
		for(int i=0; i<intData.length; ++i){
			intData[i] = data.get(i);
		}
		return intData;
	} 
	/**
	 * Read all data from a file into an float array. 
	 * @param file a file from which data is read in.
	 * @return a 1-d float array.
	 * @throws IOException
	 */
	public static float[] readFloat(String file) throws IOException{
		ArrayList<Float> data = new ArrayList<Float>(1000);
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		while(in.available()>0){
			data.add(in.readFloat());
		}
		in.close();
		float[] floatData = new float[data.size()];
		for(int i=0; i<floatData.length; ++i){
			floatData[i] = data.get(i);
		}
		return floatData;
	}
	/**
	 * Read all data from a file into an boolean array. 
	 * @param file a file from which data is read in.
	 * @return a 1-d boolean array.
	 * @throws IOException
	 */
	public static boolean[] readBoolean(String file) throws IOException{
		ArrayList<Boolean> data = new ArrayList<Boolean>(1000);
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		while(in.available()>0){
			data.add(in.readBoolean());
		}
		in.close();
		boolean[] booleanData = new boolean[data.size()];
		for(int i=0; i<booleanData.length; ++i){
			booleanData[i] = data.get(i);
		}
		return booleanData;
	} 
	/**
	 * Read a rowNumber*columnNumber double array from a file. You have to
	 * be sure that the file has enough data for reading.
	 * @param file a file from which binary data is read in.
	 * @param rowNumber the number of rows of the array.
	 * @param columnNumber the number of columns of the array.
	 * @return a 2-d double array.
	 * @throws IOException
	 */
	public static double[][] readDouble(String file, int first, int skipBytes,int rowNumber, int columnNumber, boolean byRow) throws IOException{
		if(byRow){
			return readDoubleByRow(file,first,skipBytes,rowNumber,columnNumber);
		}
		return readDoubleByColumn(file,first,skipBytes,rowNumber,columnNumber);
	}
	
	private static double[][] readDoubleByRow(String file, int first, int skipBytes,int rowNumber, int columnNumber) throws IOException{
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		double[][] data = new double[rowNumber][columnNumber];
		in.skipBytes(first);
		data[0][0] = in.readDouble();
		for(int colIndex=1; colIndex<columnNumber; ++colIndex){
			in.skipBytes(skipBytes);
			data[0][colIndex] = in.readDouble();
		}
		for(int rowIndex=1; rowIndex<rowNumber; ++rowIndex){
			for(int colIndex=0; colIndex<columnNumber; ++colIndex){
				in.skipBytes(skipBytes);
				data[rowIndex][colIndex] = in.readDouble();
			}
		}
		in.close();
		return data;
	}
	
	private static double[][] readDoubleByColumn(String file, int first, int skipBytes,int rowNumber, int columnNumber) throws IOException{
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		double[][] data = new double[rowNumber][columnNumber];
		in.skipBytes(first);
		data[0][0] = in.readDouble();
		for(int rowIndex=1; rowIndex<rowNumber; ++rowIndex){
			in.skipBytes(skipBytes);
			data[rowIndex][0] = in.readDouble();
		}
		for(int colIndex=1; colIndex<columnNumber; ++colIndex){
			for(int rowIndex=0; rowIndex<rowNumber; ++rowIndex){
				in.skipBytes(skipBytes);
				data[rowIndex][colIndex] = in.readDouble();
			}
		}
		in.close();
		return data;
	}

	public static float[][] readFloat(String file, int first, int skipBytes,int rowNumber, int columnNumber, boolean byRow) throws IOException{
		if(byRow){
			return readFloatByRow(file,first,skipBytes,rowNumber,columnNumber);
		}
		return readFloatByColumn(file,first,skipBytes,rowNumber,columnNumber);
	}
	
	private static float[][] readFloatByRow(String file, int first, int skipBytes,int rowNumber, int columnNumber) throws IOException{
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		float[][] data = new float[rowNumber][columnNumber];
		in.skipBytes(first);
		data[0][0] = in.readFloat();
		for(int colIndex=1; colIndex<columnNumber; ++colIndex){
			in.skipBytes(skipBytes);
			data[0][colIndex] = in.readFloat();
		}
		for(int rowIndex=1; rowIndex<rowNumber; ++rowIndex){
			for(int colIndex=0; colIndex<columnNumber; ++colIndex){
				in.skipBytes(skipBytes);
				data[rowIndex][colIndex] = in.readFloat();
			}
		}
		in.close();
		return data;
	}
	
	private static float[][] readFloatByColumn(String file, int first, int skipBytes,int rowNumber, int columnNumber) throws IOException{
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		float[][] data = new float[rowNumber][columnNumber];
		in.skipBytes(first);
		data[0][0] = in.readFloat();
		for(int rowIndex=1; rowIndex<rowNumber; ++rowIndex){
			in.skipBytes(skipBytes);
			data[rowIndex][0] = in.readFloat();
		}
		for(int colIndex=1; colIndex<columnNumber; ++colIndex){
			for(int rowIndex=0; rowIndex<rowNumber; ++rowIndex){
				in.skipBytes(skipBytes);
				data[rowIndex][colIndex] = in.readFloat();
			}
		}
		in.close();
		return data;
	}
	
	public static int[][] readInt(String file, int first, int skipBytes,int rowNumber, int columnNumber, boolean byRow) throws IOException{
		if(byRow){
			return readIntByRow(file,first,skipBytes,rowNumber,columnNumber);
		}
		return readIntByColumn(file,first,skipBytes,rowNumber,columnNumber);
	}
	
	private static int[][] readIntByRow(String file, int first, int skipBytes,int rowNumber, int columnNumber) throws IOException{
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		int[][] data = new int[rowNumber][columnNumber];
		in.skipBytes(first);
		data[0][0] = in.readInt();
		for(int colIndex=1; colIndex<columnNumber; ++colIndex){
			in.skipBytes(skipBytes);
			data[0][colIndex] = in.readInt();
		}
		for(int rowIndex=1; rowIndex<rowNumber; ++rowIndex){
			for(int colIndex=0; colIndex<columnNumber; ++colIndex){
				in.skipBytes(skipBytes);
				data[rowIndex][colIndex] = in.readInt();
			}
		}
		in.close();
		return data;
	}
	
	private static int[][] readIntByColumn(String file, int first, int skipBytes,int rowNumber, int columnNumber) throws IOException{
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		int[][] data = new int[rowNumber][columnNumber];
		in.skipBytes(first);
		data[0][0] = in.readInt();
		for(int rowIndex=1; rowIndex<rowNumber; ++rowIndex){
			in.skipBytes(skipBytes);
			data[rowIndex][0] = in.readInt();
		}
		for(int colIndex=1; colIndex<columnNumber; ++colIndex){
			for(int rowIndex=0; rowIndex<rowNumber; ++rowIndex){
				in.skipBytes(skipBytes);
				data[rowIndex][colIndex] = in.readInt();
			}
		}
		in.close();
		return data;
	}
	
	public static boolean[][] readBoolean(String file, int first, int skipBytes,int rowNumber, int columnNumber, boolean byRow) throws IOException{
		if(byRow){
			return readBooleanByRow(file,first,skipBytes,rowNumber,columnNumber);
		}
		return readBooleanByColumn(file,first,skipBytes,rowNumber,columnNumber);
	}
	
	private static boolean[][] readBooleanByRow(String file, int first, int skipBytes,int rowNumber, int columnNumber) throws IOException{
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		boolean[][] data = new boolean[rowNumber][columnNumber];
		in.skipBytes(first);
		data[0][0] = in.readBoolean();
		for(int colIndex=1; colIndex<columnNumber; ++colIndex){
			in.skipBytes(skipBytes);
			data[0][colIndex] = in.readBoolean();
		}
		for(int rowIndex=1; rowIndex<rowNumber; ++rowIndex){
			for(int colIndex=0; colIndex<columnNumber; ++colIndex){
				in.skipBytes(skipBytes);
				data[rowIndex][colIndex] = in.readBoolean();
			}
		}
		in.close();
		return data;
	}
	
	private static boolean[][] readBooleanByColumn(String file, int first, int skipBytes,int rowNumber, int columnNumber) throws IOException{
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		boolean[][] data = new boolean[rowNumber][columnNumber];
		in.skipBytes(first);
		data[0][0] = in.readBoolean();
		for(int rowIndex=1; rowIndex<rowNumber; ++rowIndex){
			in.skipBytes(skipBytes);
			data[rowIndex][0] = in.readBoolean();
		}
		for(int colIndex=1; colIndex<columnNumber; ++colIndex){
			for(int rowIndex=0; rowIndex<rowNumber; ++rowIndex){
				in.skipBytes(skipBytes);
				data[rowIndex][colIndex] = in.readBoolean();
			}
		}
		in.close();
		return data;
	}
//	/**
//	 * Read a 3-d double array from a file. You have to make sure that
//	 * the file has enough data for reading.
//	 * @param file a file from which data is read in.
//	 * @param d1 the first dimension of the array.
//	 * @param d2 the second dimension of the array.
//	 * @param d3 the third dimension of the array.
//	 * @return a 3-d double array.
//	 * @throws IOException
//	 */
//	public static double[][][] readDouble(String file, int first, int skipBytes, int d1, int d2, int d3) throws IOException{
//		double[][][] data = new double[d1][d2][d3];
//		DataInputStream in = new DataInputStream(new FileInputStream(file));
//		in.skipBytes(first);
//		data[0][0][0] = in.readDouble();
//		for(int d2Index)
//		for(int i=0; i<d1; ++i){
//			for(int j=0; j<d2; ++j){
//				for(int k=0; k<d3; ++k){
//					data[i][j][k] = in.readDouble();
//				}
//			}
//		}
//		in.close();
//		return data;
//	}
//	/**
//	 * Read a 3-d float array from a file. You have make sure that
//	 * the file has enough data for reading.
//	 * @param file a file from which data is read in.
//	 * @param d1 the first dimension of the array.
//	 * @param d2 the second dimension of the array.
//	 * @param d3 the third dimension of the array.
//	 * @return a 3-d float array.
//	 * @throws IOException
//	 */
//	public static float[][][] readFloat(String file, int d1, int d2, int d3) throws IOException{
//		float[][][] data = new float[d1][d2][d3];
//		DataInputStream in = new DataInputStream(new FileInputStream(file));
//		for(int i=0; i<d1; ++i){
//			for(int j=0; j<d2; ++j){
//				for(int k=0; k<d3; ++k){
//					data[i][j][k] = in.readFloat();
//				}
//			}
//		}
//		in.close();
//		return data;
//	}
//	/**
//	 * Read a 3-d integer array from a file. You have make sure that
//	 * the file has enough data for reading.
//	 * @param file a file from which data is read in.
//	 * @param d1 the first dimension of the array.
//	 * @param d2 the second dimension of the array.
//	 * @param d3 the third dimension of the array.
//	 * @return a 3-d integer array.
//	 * @throws IOException
//	 */
//	public static int[][][] readInt(String file, int first, int skipBytes, int d1, int d2, int d3) throws IOException{
//		int[][][] data = new int[d1][d2][d3];
//		DataInputStream in = new DataInputStream(new FileInputStream(file));
//		for(int i=0; i<d1; ++i){
//			for(int j=0; j<d2; ++j){
//				for(int k=0; k<d3; ++k){
//					data[i][j][k] = in.readInt();
//				}
//			}
//		}
//		in.close();
//		return data;
//	}
//	/**
//	 * Read a 3-d boolean array from a file. You have make sure that
//	 * the file has enough data for reading.
//	 * @param file a file from which data is read in.
//	 * @param d1 the first dimension of the array.
//	 * @param d2 the second dimension of the array.
//	 * @param d3 the third dimension of the array.
//	 * @return a 3-d boolean array.
//	 * @throws IOException
//	 */
//	public static boolean[][][] readBoolean(String file, int d1, int d2, int d3) throws IOException{
//		boolean[][][] data = new boolean[d1][d2][d3];
//		DataInputStream in = new DataInputStream(new FileInputStream(file));
//		for(int i=0; i<d1; ++i){
//			for(int j=0; j<d2; ++j){
//				for(int k=0; k<d3; ++k){
//					data[i][j][k] = in.readBoolean();
//				}
//			}
//		}
//		in.close();
//		return data;
//	}
}
